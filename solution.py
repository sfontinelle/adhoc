# slcsp

import csv
import sqlite3

con = None


def setup_database():
    # db is in memory so leave the connection open
    global con
    con = sqlite3.connect(":memory:")
    cur = con.cursor()

    slcsp_location = "slcsp.csv"
    plans_location = "plans.csv"
    zips_location = "zips.csv"

    # http: // stackoverflow.com / a / 2888042
    # slcsp
    cur.execute("CREATE TABLE slcsp (zipcode, rate);")

    with open(slcsp_location, 'rt') as fin:
        dr = csv.DictReader(fin)
        to_db = [(i['zipcode'], i['rate']) for i in dr]

    cur.executemany("INSERT INTO slcsp (zipcode, rate) VALUES (?, ?);", to_db)
    con.commit()

    # plans
    cur.execute("CREATE TABLE plans (plan_id, state, metal_level, rate, rate_area);")

    with open(plans_location, 'rt') as fin:
        dr = csv.DictReader(fin)
        to_db = [(i['plan_id'], i['state'], i['metal_level'], i['rate'], i['rate_area']) for i in dr]

    cur.executemany("INSERT INTO plans (plan_id, state, metal_level, rate, rate_area) VALUES (?, ?, ?, ?, ?);", to_db)
    con.commit()

    # zips
    cur.execute("CREATE TABLE zips (zipcode, state, county_code, name, rate_area);")

    with open(zips_location, 'rt') as fin:

        dr = csv.DictReader(fin)
        to_db = [(i['zipcode'], i['state'], i['county_code'], i['name'], i['rate_area']) for i in dr]

    cur.executemany("INSERT INTO zips (zipcode, state, county_code, name, rate_area) VALUES (?, ?, ?, ?, ?);", to_db)
    con.commit()


def get_zipcodes():
    cur = con.cursor()
    cur.execute("SELECT zipcode from slcsp;")
    zipcodes = []
    for row in cur:
        zipcodes.append(row[0])
    return zipcodes


def find_rate(zipcode):

        rate = None

        #get state and rate area
        cur = con.cursor()
        cur.execute("Select state, rate_area from zips where zipcode = ?", (zipcode,))

        row = cur.fetchone()
        first_rate_area = (row[0], row[1])
        is_ambiguous = False
        for row in cur:
            rate_area = (row[0], row[1])
            if rate_area != first_rate_area:
                is_ambiguous = True
                break

        if not is_ambiguous:
            # lookup silver plans in rate area and choose plan w/ slcsp
            cur.execute("Select rate from plans "
                        "where metal_level = 'Silver' "
                        "and state = ? "
                        "and rate_area = ?"
                        "order by rate asc "
                        "limit 1,1",
                        (first_rate_area[0], first_rate_area[1]))
            row = cur.fetchone()
            if row is not None:
                rate = row[0]

        return rate


def save_rate(zipcode, rate):
    cur = con.cursor()
    cur.execute("update slcsp set rate= ? where zipcode= ?", (rate, zipcode))


def export_table(table_name):
    filename = table_name + ".csv"

    cur = con.cursor()
    cur.execute("Select * from slcsp")

    # http: // stackoverflow.com / a / 3765652
    with open(filename, "w") as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow([i[0] for i in cur.description])  # write headers
        csv_writer.writerows(cur)


def main():

    setup_database()
    zipcodes = get_zipcodes()
    for zipcode in zipcodes:
        rate = find_rate(zipcode)
        if rate is not None:
            save_rate(zipcode, rate)
    export_table("slcsp")


if __name__ == "__main__":
    main()
